const express = require('express');
const { join } = require('path');
const app = express();
if (process.env.NODE_ENV === 'production') {
  app.use((req, res, next) => {
    if (req.header('x-forwarded-proto') !== 'https' && !req.header('host').includes('localhost')) {
      res.redirect(`https://${req.header('host')}${req.url}`);
    } else {
      next();
    }
  });
}
app.use(express.static(join(__dirname, 'build')));
console.log('process.env.NODE_ENV: ', process.env.NODE_ENV);
// use the gzipped bundle
app.get('*.js', (req, res, next) => {
  req.url = req.url + '.gz'; // eslint-disable-line
  res.set('Content-Encoding', 'gzip');
  next();
});

app.listen(process.env.PORT || 5000);
